const mongoose = require('mongoose');
async function connect() {
    try {
        await mongoose.connect(
          "mongodb://mongo-service.default.svc.cluster.local:27017/final-project",
          {
            useNewUrlParser: true,
            useUnifiedTopology: true,
          }
        );
        // console.log('Connect successfully!')
    } catch (e) {
        // console.log('Connect failure!')
    }
}

module.exports = { connect }